#include "ControllerMain.h"


	// Flag d'exécution
	static bool ExecutionFlag = true;


// *********************************************************************************************************************
	

	//
	// Programme principal
	int main(int argc, const char** argv)
	{
		
		// Déclarations
		string command;
		Controller* controller = NULL;
		
		
		// Paramétrage de la sortie
		cout << std::setprecision(4) << std::fixed;
		
		// Sécurité
		try
		{
			
			// Initialisation
			controller = new Controller("Controller.config");
			controller->Start();
			
			// Configuration du handler pour la terminaison
			signal(SIGABRT, SignalHandler);
			signal(SIGTERM, SignalHandler);
			signal(SIGINT, SignalHandler);
			
			// Boucle de travail
			while (ExecutionFlag && controller->IsRunning())
			{
				usleep(1000 * 100);
			}
			
			
			// Nettoyage
			controller->Stop();
			delete controller;
			
		}
		catch (string exc)
		{
		
			// Affichage de l'erreur
			cout << "## Exception raised, unable to continue execution, exiting" << endl;
			cout << exc << endl << endl;
		
			// Nettoyage
			if (controller != NULL) delete controller;
			
			// Retour sur erreur
			return -1;
		
		}
		
		
		// Retour OK
		return 0;
		
	}


	//
	// Handler pour les signaux de terminaison
	void SignalHandler(int signal)
	{
		ExecutionFlag = false;
		cin.setstate(std::ios_base::eofbit);
	}
	
	