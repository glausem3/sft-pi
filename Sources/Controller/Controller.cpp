#include "Controller.h"


// *********************************************************************************************************************


	//
	// Point d'entrée du lanceur du thread d'exécution
	void* Controller::Execution_Thread_Bootstraper(void* instance)
	{
	
		// Sécurité
		try
		{
		
			// Exécution
			((Controller*)instance)->Execution_Thread();
			
		} catch (string exc)
		{
		
			// Mise à jour du flag d'erreur
			((Controller*)instance)->errorFlag = true;
			((Controller*)instance)->error = exc;
		
		}
		
		// Mise à jour du flag de fin
		((Controller*)instance)->terminatedFlag = true;
		
		// Retour
		return NULL;
		
	}
	
	
	//
	// Point d'entrée du thread d'exécution
	void Controller::Execution_Thread()
	{
	
		// Déclarations
		int state;
		float delta;
		float sensorValue, exposureValue;
		Effect* fallSound;
		Effect* hitSound;
		Music* backgroundSound = NULL;
		
		
		// Initialisation
		srand(getpid());
		exposureValue = 0.0f;
	
		// Boucle de travail
		while (executionFlag)
		{
	
			// Musique de fond
			if (backgroundSound == NULL || !backgroundSound->IsPlaying())
			{
				backgroundSound = backgroundSounds.at(rand() % backgroundSounds.size());
				backgroundSound->Play();
				cout << "> Background changed" << endl;
			}
			
			// Action selon l'état
			switch (state)
			{
				
				// En attente d'un événement extérieur
				case 0:
				
					// Lecture du capteur
					sensorValue = board->GetSensorValueNormalized(configuration.SensorIndex);
					
					// Mise à jour de l'exposition
					exposureValue = (
						((1.0f - configuration.ExposureChangeSpeed) * exposureValue) +
						(configuration.ExposureChangeSpeed * sensorValue)
					);
					exposureValue = max(exposureValue, sensorValue);
					
					// Calcul du delta
					delta = max(0.0f, exposureValue - sensorValue);
				
					//cout << sensorValue << "  /  " << exposureValue << "  /  " << delta << endl;
				
					// Trigger venant du capteur, lancement du son de chute
					if (delta >= configuration.DeltaThreshold)
					{
						fallSound = fallSounds.at(rand() % fallSounds.size());
						fallSound->Play();
						state = 1;
						cout << "> Falling" << endl;
					}
				
					break;
				
				
				// Chute en cours
				case 1:
				
					// Fin de chute, lancement du son de l'impact
					if (!fallSound->IsPlaying())
					{
						fallSound = NULL;
						hitSound = hitSounds.at(rand() % hitSounds.size());
						hitSound->Play();
						state = 2;
						cout << "> Hit" << endl;
					}
					
					break;
					
					
				// Impact en cours
				case 2:
				
					// Fin d'impact, retour à l'attente
					if (!hitSound->IsPlaying())
					{
						hitSound = NULL;
						state = 0;
						cout << "> Idle" << endl;
						exposureValue = 0.0f;
					}
				
					break;
			
			
			}
			
			
			// Temps d'attente
			usleep(1000 * configuration.PollingInterval);
			
		}
	
	}


// *********************************************************************************************************************


	//
	// Constructeur avec fichier de configuration
	Controller::Controller(string configurationFile)
	{
	
		// Déclarations
		Effect* effect;
		Music* music;
	
		
		// Initialisation des champs
		executionFlag = false;
		terminatedFlag = false;
		errorFlag = false;
		error = "";
		
		// Chargement de la configuration
		configuration = Configuration::FromFile(configurationFile);
		
		// Vérification des listes de sons
		if (configuration.FallSounds.size() == 0)
			throw string("Controller::ctor() There must be at least one fall sound in configuration");
		if (configuration.HitSounds.size() == 0)
			throw string("Controller::ctor() There must be at least one hit sound in configuration");
		if (configuration.BackgroundSounds.size() == 0)
			throw string("Controller::ctor() There must be at least one background sound in configuration");
		
		// Initialisation d'OpenAL
		OpenAL::Initialize();
		OpenAL::SetGlobalVolume(configuration.GlobalVolume);
		
		// Chargement des sons
		for (unsigned int i=0; i<configuration.FallSounds.size(); i++)
		{
			effect = Effect::FromVorbisFile(configuration.FallSounds[i]);
			effect->SetVolume(configuration.FallSoundVolume);
			fallSounds.push_back(effect);
		}
		for (unsigned int i=0; i<configuration.HitSounds.size(); i++)
		{
			effect = Effect::FromVorbisFile(configuration.HitSounds[i]);
			effect->SetVolume(configuration.HitSoundVolume);
			hitSounds.push_back(effect);
		}
		for (unsigned int i=0; i<configuration.BackgroundSounds.size(); i++)
		{
			music = Music::FromVorbisFile(configuration.BackgroundSounds[i]);
			music->SetVolume(configuration.BackgroundSoundVolume);
			backgroundSounds.push_back(music);
		}
		
		
		// Connexion à la carte phidget
		board = new InterfaceKit(configuration.BoardIndex, 5000);
		//board = new DummyInterfaceKit();
		
	}
	
	
	//
	// Destructeur
	Controller::~Controller()
	{
		
		// Arrêt du thread d'exécution
		Stop();
		
		// Nettoyage des sons
		for (unsigned int i=0; i<backgroundSounds.size(); i++)
			delete backgroundSounds.at(i);
		for (unsigned int i=0; i<fallSounds.size(); i++)
			delete fallSounds.at(i);
		for (unsigned int i=0; i<hitSounds.size(); i++)
			delete hitSounds.at(i);
		
		// Nettoyage
		OpenAL::Terminate();
		delete board;
		
	}
	
	
	//
	// Démarrer le contrôleur
	void Controller::Start()
	{
		if (!executionFlag)
		{
			executionFlag = true;
			terminatedFlag = false;
			errorFlag = false;
			error = "";
			pthread_create(&executionThread, NULL, Execution_Thread_Bootstraper, this);
		}
	}
	
	
	//
	// Arrêter le contrôleur
	void Controller::Stop()
	{
		if (executionFlag)
		{
			executionFlag = false;
			pthread_join(executionThread, NULL);
		}
	}
	
	