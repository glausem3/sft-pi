#include "InterfaceKit.h"

// *********************************************************************************************************************
	

	//
	// Constructeur avec n° de carte physique
	InterfaceKit::InterfaceKit(int boardIndex, int timeout)
	{
	
		// Déclarations
		int result;
		
	
		// Initialisation
		CPhidgetInterfaceKit_create(&handle);
		CPhidget_open((CPhidgetHandle)handle, max(-1, boardIndex));
		
		// Connexion à la carte
		result = CPhidget_waitForAttachment((CPhidgetHandle)handle, timeout);
		
		// Erreur de connexion
		if (result != EPHIDGET_OK)
		{
			this->~InterfaceKit();
			throw string("InterfaceKit::ctor() Unable to connect to board");
		}
	
		// Lecture des infos de la carte
		CPhidgetInterfaceKit_getSensorCount(handle, &sensorsCount);
		CPhidgetInterfaceKit_getInputCount(handle, &inputsCount);
		CPhidgetInterfaceKit_getOutputCount(handle, &outputsCount);
		
	}
	
	
	//
	// Destructeur
	InterfaceKit::~InterfaceKit()
	{
		if (this->handle >= 0)
		{
			CPhidget_close((CPhidgetHandle)this->handle);
			CPhidget_delete((CPhidgetHandle)this->handle);
			this->handle = (CPhidgetInterfaceKitHandle)-1;
		}
	}
	
	
	//
	// Obtenir la valeur d'un capteur (intervalle 0,1000)
	int InterfaceKit::GetSensorValue(int index)
	{
	
		// Déclarations
		int value;
	
		// Vérification de l'index
		if (index < 0 || index >= sensorsCount)
			throw string("InterfaceKit::GetSensorValue() index is out of range 0," + (sensorsCount-1));
	
		// Lecture
		CPhidgetInterfaceKit_getSensorValue(handle, index, &value);
		
		// Retour de la valeur
		return max(0, min(1000, value));
	
	}
	
	
	//
	// Obtenir la valeur d'un capteur sous forme normalisée (intervalle 0,1)
	float InterfaceKit::GetSensorValueNormalized(int index)
	{
		return GetSensorValue(index) / 1000.0f;
	}
	
	
	//
	// Obtenir l'état d'une entrée
	bool InterfaceKit::GetInput(int index)
	{
	
		// Déclarations
		int value;
	
		// Vérification de l'index
		if (index < 0 || index >= inputsCount)
			throw string("InterfaceKit::GetInput() index is out of range 0," + (inputsCount-1));
	
		// Lecture
		CPhidgetInterfaceKit_getInputState(handle, index, &value);
		
		// Retour de la valeur
		return (value == PTRUE);
	
	}
	
	
	//
	// Obtenir l'état d'une sortie
	bool InterfaceKit::GetOutput(int index)
	{
	
		// Déclarations
		int value;
	
		// Vérification de l'index
		if (index < 0 || index >= outputsCount)
			throw string("InterfaceKit::GetOutput() index is out of range 0," + (outputsCount-1));
	
		// Lecture
		CPhidgetInterfaceKit_getOutputState(handle, index, &value);
		
		// Retour de la valeur
		return (value == PTRUE);
		
	}
	
	
	//
	// Définir l'état d'une sortie
	void InterfaceKit::SetOutput(int index, bool state)
	{
	
		// Vérification de l'index
		if (index < 0 || index >= outputsCount)
			throw string("InterfaceKit::GetOutput() index is out of range 0," + (outputsCount-1));
	
		// Lecture
		CPhidgetInterfaceKit_setOutputState(handle, index, (state ? PTRUE : PFALSE));
	
	}
	