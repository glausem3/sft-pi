#pragma once

// Includes standards
#include <algorithm>
#include <string>

// Includes Phidgets
#include <phidget21.h>

using namespace std;

// *********************************************************************************************************************


//
// Classe wrapper pour l'accès à la carte InterfaceKit
//
class InterfaceKit
{

	// Nombre de capteurs
	int sensorsCount;
	
	// Nombre d'entrées
	int inputsCount;
	
	// Nombre de sorties
	int outputsCount;

	// Handle vers la carte
	CPhidgetInterfaceKitHandle handle;	


// *********************************************************************************************************************
public:


	//
	// Constructeur avec n° de carte physique
	//
	//   # Paramètres
	//     <boardIndex>  n° de la carte physique ou -1 pour utiliser la première carte disponible
	//     <timeout>     délai d'attente maximum en millisecondes pour la connexion à la carte
	//
	InterfaceKit(int boardIndex, int timeout);
	
	
	//
	// Destructeur
	~InterfaceKit();


	//
	// Obtenir le nombre de capteurs sur la carte
	int GetSensorsCount() { return sensorsCount; }
	
	
	//
	// Obtenir le nombre d'entrées sur la carte
	int GetInputsCount() { return inputsCount; }
	
	
	//
	// Obtenir le nombre de sorties sur la carte
	int GetOutputsCount() { return outputsCount; }
	
	
	//
	// Obtenir la valeur d'un capteur (intervalle 0,1000)
	int GetSensorValue(int index);
	
	
	//
	// Obtenir la valeur d'un capteur sous forme normalisée (intervalle 0,1)
	float GetSensorValueNormalized(int index);
	
	
	//
	// Obtenir l'état d'une entrée
	bool GetInput(int index);
	
	
	//
	// Obtenir l'état d'une sortie
	bool GetOutput(int index);
	
	
	//
	// Définir l'état d'une sortie
	void SetOutput(int index, bool state);

};
