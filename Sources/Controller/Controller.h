#pragma once

// Includes standards
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <string>
#include <unistd.h>
#include <cstdlib>
#include <errno.h>

// Includes locaux
#include "Configuration.h"
#include "Sound/OpenAL.h"
#include "Sound/Effect.h"
#include "Sound/Music.h"
#include "Phidget/InterfaceKit.h"

using namespace std;

// *********************************************************************************************************************
#include <cmath>
struct DummyInterfaceKit
{

	
	float GetSensorValueNormalized(int index)
	{
	
		static int time = 0;
	
		time++;
		
		return sin(time / 40.0f) * 0.5f + 0.5f;
	
		
	}

};


//
// Classe du contrôleur
//
class Controller
{

	// Configuration
	Configuration configuration;

	// Sons (fond, chute et impact)
	vector<Music*> backgroundSounds;
	vector<Effect*> fallSounds;
	vector<Effect*> hitSounds;

	// Carte phidget sur laquelle est branché le capteur
	InterfaceKit* board;
	//DummyInterfaceKit* board;
	
	// Thread d'exécution
	pthread_t executionThread;
	
	// Flags d'exécution
	volatile bool executionFlag;
	volatile bool terminatedFlag;
	volatile bool errorFlag;
	
	// Dernière erreur lors de l'exécution
	string error;
	
	
	
	//
	// Point d'entrée du lanceur du thread d'exécution
	static void* Execution_Thread_Bootstraper(void* instance);
	
	//
	// Point d'entrée du thread d'exécution
	void Execution_Thread();
	
	

// *********************************************************************************************************************
public:


	//
	// Constructeur avec fichier de configuration
	Controller(string configurationFile);
	
	
	//
	// Destructeur
	~Controller();
	
	
	//
	// Démarrer le contrôleur
	void Start();
	
	
	//
	// Arrêter le contrôleur
	void Stop();
	
	
	
	//
	// Déterminer si le contrôleur est en cours d'exécution
	bool IsRunning() { return executionFlag & !terminatedFlag; }
	
	
	//
	// Déterminer si le contrôleur s'est terminé en état d'erreur
	bool CheckError() { return errorFlag; }


	//
	// Obtenir l'erreur ayant provoqué l'arrêt en erreur du contrôleur
	string GetError() { return error; }

};
