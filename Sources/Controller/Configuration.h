#pragma once

// Include standards
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>
#include <cstdlib>

using namespace std;

// *********************************************************************************************************************


//
// Classe conteneur pour la configuration d'un contrôleur
class Configuration
{

	//
	// Convertir une chaîne en valeur entière
	static int ToInteger(string value, int minValue, int maxValue)
	{
		
		// Déclarations
		int result;
		
		// Lecture
		stringstream stream(value);
		stream >> result;
		if ((stream.rdstate() & std::ifstream::failbit ) != 0) 
			throw string("Configuration::ToInteger() Invalid integer value: " + value);
			
		// Retour de la valeur
		return max(minValue, min(maxValue, result));
		
	}
	
	
	//
	// Convertir une chaîne en valeur flottante
	static float ToFloat(string value, float minValue, float maxValue)
	{
	
		// Déclarations
		float result;
		
		// Lecture
		stringstream stream(value);
		stream >> result;
		if ((stream.rdstate() & std::ifstream::failbit ) != 0) 
			throw string("Configuration::ToFloat() Invalid float value: " + value);
			
		// Retour de la valeur
		return max(minValue, min(maxValue, result));
		
	}

		
// *********************************************************************************************************************
public:



	// Intervalle de polling du capteur en ms
	int PollingInterval;
	
	
	// Vitesse d'ajustement de l'exposition [0,1]
	//   (plus la valeur est élevée, plus l'adaptation sera rapide)
	float ExposureChangeSpeed;
	
	
	// Seuil de différence entre la valeur du capteur et la valeur d'exposition pour le déclenchement du son
	float DeltaThreshold;
	
	
	// Index de la carte phidget à utiliser (-1 pour utiliser première carte disponible)
	int BoardIndex;
	
	
	// Index du capteur à utiliser sur la carte phidget
	int SensorIndex;
	
	
	// Volume sonore global
	float GlobalVolume;
	
	
	// Volume des sons de chute
	float FallSoundVolume;
	
	
	// Volume des sons d'impact
	float HitSoundVolume;
	
	
	// Volume des sons de fond
	float BackgroundSoundVolume;
	
	
	// Liste des sons de chute
	vector<string> FallSounds;
	
	// Liste des sons d'impact
	vector<string> HitSounds;
	
	// Liste des sons de fond
	vector<string> BackgroundSounds;


	//
	// Constructeur avec valeurs par défaut
	Configuration()
	{
		PollingInterval = 50;
		ExposureChangeSpeed = 0.05f;
		DeltaThreshold = 0.2f;
		BoardIndex = -1;
		SensorIndex = 0;
		GlobalVolume = 1.0f;
		FallSoundVolume = 1.0f;
		HitSoundVolume = 1.0f;
		BackgroundSoundVolume = 1.0f;
	}



	//
	// Charger une configuration depuis un fichier
	static Configuration FromFile(string filename)
	{
		
		// Déclarations
		int lineIndex;
		int separatorIndex;
		ifstream sourceStream;
		Configuration configuration;
		string currentLine;
		string entryKey, entryValue;
		
		
		// Initialisation
		lineIndex = 1;
		
		// Ouverture du fichier
		sourceStream.open(filename.c_str());
		if (!sourceStream.is_open())
			throw string("Configuration::FromFile() Unable to open file " + filename);
	
		// Lecture du contenu du fichier
		while (!sourceStream.eof())
		{
			lineIndex++;
			try
			{
		
				// Lecture de la ligne courante
				getline(sourceStream, currentLine);
				currentLine.erase(
					currentLine.begin(),
					std::find_if(currentLine.begin(),
					currentLine.end(),
					std::not1(std::ptr_fun<int, int>(std::isspace)))
				);
			
				// Ligne vide ou commentaire, on ignore
				if (currentLine == "") continue;
				if (currentLine.at(0) == '#') continue;
				
				// Extraction de la clé et de la valeur brute
				separatorIndex = currentLine.find("=");
				if (separatorIndex == (int)string::npos)
					throw string("missing '='");
				entryKey = currentLine.substr(0, separatorIndex);
				entryValue = currentLine.substr(separatorIndex+1, currentLine.length() - separatorIndex - 1);
				
				// Interprétation de la clé
				if (entryKey == "PollingInterval")
					configuration.PollingInterval = ToInteger(entryValue, 10, 1000);
				else if (entryKey == "ExposureChangeSpeed")
					configuration.ExposureChangeSpeed = ToFloat(entryValue, 0.0f, 1.0f);
				else if (entryKey == "DeltaThreshold")
					configuration.DeltaThreshold = ToFloat(entryValue, 0.001f, 1.0f);
				else if (entryKey == "BoardIndex")
					configuration.BoardIndex = ToInteger(entryValue, -1, 255);
				else if (entryKey == "SensorIndex")
					configuration.SensorIndex = ToInteger(entryValue, 0, 255);
				else if (entryKey == "GlobalVolume")
					configuration.GlobalVolume = ToFloat(entryValue, 0.0f, 10.0f);
				else if (entryKey == "FallSoundVolume")
					configuration.FallSoundVolume = ToFloat(entryValue, 0.0f, 10.0f);
				else if (entryKey == "HitSoundVolume")
					configuration.HitSoundVolume = ToFloat(entryValue, 0.0f, 10.0f);
				else if (entryKey == "BackgroundSoundVolume")
					configuration.BackgroundSoundVolume = ToFloat(entryValue, 0.0f, 1.0f);
				else if (entryKey == "FallSound")
					configuration.FallSounds.push_back(entryValue);
				else if (entryKey == "HitSound")
					configuration.HitSounds.push_back(entryValue);
				else if (entryKey == "BackgroundSound")
					configuration.BackgroundSounds.push_back(entryValue);
				
			}
			catch (string exc)
			{
				cout << "Configuration::FromFile() [";
				cout << filename << ":" << lineIndex << "] Line ignored: " << exc << endl;
			}
		}
	
		/*cout << "PollingInterval = " << configuration.PollingInterval << endl;
		cout << "ExposureChangeSpeed = " << configuration.ExposureChangeSpeed << endl;
		cout << "DeltaThreshold = " << configuration.DeltaThreshold << endl;
		cout << "BoardIndex = " << configuration.BoardIndex << endl;
		cout << "SensorIndex = " << configuration.SensorIndex << endl;
		cout << "GlobalVolume = " << configuration.GlobalVolume << endl;
		cout << "FallSoundVolume = " << configuration.FallSoundVolume << endl;
		cout << "HitSoundVolume = " << configuration.HitSoundVolume << endl;
		cout << "BackgroundSoundVolume = " << configuration.BackgroundSoundVolume << endl;
		
		cout << "FallSounds = {" << endl;
		for (unsigned int i=0; i<configuration.FallSounds.size(); i++)
			cout << "  " << configuration.FallSounds[i] << endl;
		cout << "}" << endl;
	
		cout << "HitSounds = {" << endl;
		for (unsigned int i=0; i<configuration.HitSounds.size(); i++)
			cout << "  " << configuration.HitSounds[i] << endl;
		cout << "}" << endl;
		
		cout << "BackgroundSounds = {" << endl;
		for (unsigned int i=0; i<configuration.BackgroundSounds.size(); i++)
			cout << "  " << configuration.BackgroundSounds[i] << endl;
		cout << "}" << endl;*/
	
		// Fermeture du fichier
		sourceStream.close();
		
		// Retour de la configuration
		return configuration;
		
	}
	
};
