#include "OpenAL.h"


// *********************************************************************************************************************


	// Device système
	ALCdevice* OpenAL::device = NULL;
	
	// Contexte global
	ALCcontext* OpenAL::context = NULL;

	// Flags d'état
	bool OpenAL::initialized = false;


// *********************************************************************************************************************


	//
	// Initialiser OpenAL
	void OpenAL::Initialize()
	{
		
		// Déjà initialisé
		if (initialized) return;
		
		// Création du device
        device = alcOpenDevice(NULL);
        if (device == NULL)
			throw string("OpenAL::Initialize() Unable to open device");

		// Création du contexte
		context = alcCreateContext(device, NULL);
		if (context == NULL)
		{
			alcCloseDevice(device);
			device = NULL;
			throw string("OpenAL::Initialize() Unable to create context");
		}
		
		// Activation du contexte
		alcMakeContextCurrent(context);
		
		// Mise à jour de l'état
		initialized = true;
		
	}
	
	
	//
	// Terminer et nettoyer les ressources utilisées par OpenAL
	void OpenAL::Terminate()
	{
	
		// Pas initialisé
		if (!initialized) return;
	
		// Nettoyage du contexte
		alcMakeContextCurrent(NULL);
		alcDestroyContext(context);
		context = NULL;
		
		// Nettoyage du device
		alcCloseDevice(device);
		device = NULL;
	
	}


	//
	// Obtenir le volume global
	float OpenAL::GetGlobalVolume()
	{
	
		// Déclarations
		ALfloat value;
		
		// Récupération
		alGetListenerf(AL_GAIN, &value);
		alGetError();
		
		// Retour
		return value;
		
	}
	
	
	//
	// Définir le volume global
	void OpenAL::SetGlobalVolume(float value)
	{
		alListenerf(AL_GAIN, value);
		alGetError();
	}
	
	
	//
	// Obtenir la position de l'écouteur dans l'espace
	void OpenAL::GetListenerPosition(float position[3])
	{
		alGetListenerfv(AL_POSITION, position);
		alGetError();
	}
	
	//
	// Définir la position de l'écouteur dans l'espace
	void OpenAL::SetListenerPosition(float position[3])
	{
		alListenerfv(AL_POSITION, position);
		alGetError();
	}
	
	void OpenAL::SetListenerPosition(float x, float y, float z)
	{
		alListener3f(AL_POSITION, x, y, z);
		alGetError();
	}
	

	//
	// Obtenir la vélocité de l'écouteur dans l'espace
	void OpenAL::GetListenerVelocity(float velocity[3])
	{
		alGetListenerfv(AL_VELOCITY, velocity);
		alGetError();
	}
	
	//
	// Définir la vélocité de l'écouteur dans l'espace
	void OpenAL::SetListenerVelocity(float velocity[3])
	{
		alListenerfv(AL_VELOCITY, velocity);
		alGetError();
	}
	
	void OpenAL::SetListenerVelocity(float x, float y, float z)
	{
		alListener3f(AL_VELOCITY, x, y, z);
		alGetError();
	}

