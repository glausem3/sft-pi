#pragma once

// Includes standards
#include <iostream>
#include <algorithm>
#include <string>
#include <unistd.h>

// Includes pour OpenAL
#include <AL/al.h>
#include <AL/alc.h>


using namespace std;

// *********************************************************************************************************************



//
// Classe de base des sons
//
class Sound
{

	// Source associée au son
	ALuint source;
	
	// Nombre de canaux du son
	int channelsCount;
	
	// Taux d'échantillonage
	int sampleRate;
	
	// Durée en secondes
	float duration;


// *********************************************************************************************************************
protected:



	//
	// Constructeur protégé avec infos
	Sound(int sampleRate, int channelsCount, float duration);

	
	//
	// Obtenir la source du son
	ALuint
	 GetSource() { return source; }

	//
	// Obtenir l'état de la source
	ALenum GetSourceState();
				

// *********************************************************************************************************************
public:


	//
	// Destructeur
	virtual ~Sound();


	//
	// Lancer la lecture
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual void Play(bool looping = false);
	
	
	//
	// Mettre la lecture en pause ou la reprendre si elle l'est déjà
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual void Pause();
	
	
	//
	// Arrêter la lecture
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual void Stop();
	
	
	//
	// Déterminer si le son est en cours de lecture
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual bool IsPlaying();
	
	
	//
	// Déterminer si le son est en pause
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual bool IsPaused();
	
	
	//
	// Déterminer si le son est arrêté
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual bool IsStopped();
	
	
	//
	// Retourner le temps de lecture en secondes
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual float GetPlayTime();
	
	
	//
	// Positionner la lecture au temps spécififé en secondes
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual void SetPlayTime(float value);
	
	
	//
	// Obtenir le nombre de canaux du son
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual int GetChannelsCount() { return channelsCount; }
	
	
	//
	// Obtenir le taux d'échantillonage du son
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual int GetSampleRate() { return sampleRate; }
	
	
	//
	// Obtenir la durée totale en secondes du son
	//   > redéfinissable pour personnaliser le comportement par défaut
	//
	virtual float GetDuration() { return duration; }
	
	
	
	//
	// Obtenir le volume
	float GetVolume();
	
	
	//
	// Définir le volume
	void SetVolume(float value);
	
	
	//
	// Obtenir la position du son dans l'espace
	void GetPosition(float position[3]);
	
	
	//
	// Définir la position du son dans l'espace
	void SetPosition(float position[3]);
	void SetPosition(float x, float y=0.0f, float z=0.0f);


	//
	// Obtenir le pitch du son
	float GetPitch();
	
	
	//
	// Définir le pitch du son
	void SetPitch(float value);
	

};
