#include "Sound.h"


// *********************************************************************************************************************


	//
	// Constructeur par défaut protégé
	Sound::Sound(int sampleRate, int channelsCount, float duration)
	{
		
		// Génération de la source
		alGenSources(1, &source);
		if (alGetError() != AL_NO_ERROR)
			throw string("Sound::ctor() Unable to create source");
		
		// Mise à jour des champs
		this->sampleRate = sampleRate;
		this->channelsCount = channelsCount;
		this->duration = duration;
		
	}
	
	
	//
	// Obtenir l'état de la source
	ALenum Sound::GetSourceState()
	{
	
		// Déclarations
		ALenum state;
		
		// Lecture de l'état
		alGetSourcei(source, AL_SOURCE_STATE, &state);
		alGetError();
		
		// Retour de l'état
		return state;
	
	}

// *********************************************************************************************************************


	//
	// Destructeur
	Sound::~Sound()
	{
		alDeleteSources(1, &source);
		alGetError();
	}


	//
	// Lancer la lecture
	void Sound::Play(bool looping)
	{
		alSourcePlay(source);
		alSourcei(source, AL_LOOPING, (looping ? AL_TRUE : AL_FALSE));
		alGetError();
	}
	
	
	//
	// Mettre la lecture en pause
	void Sound::Pause()
	{
		if (IsPaused())
			alSourcePlay(source);
		else
			alSourcePause(source);
		alGetError();
	}
	
	
	//
	// Arrêter la lecture
	void Sound::Stop()
	{
		alSourceStop(source);
		alGetError();
	}
	
	
	//
	// Déterminer si le son est en cours de lecture
	bool Sound::IsPlaying()
	{
		return (GetSourceState() == AL_PLAYING);
	}
	
	
	//
	// Déterminer si le son est en pause
	bool Sound::IsPaused()
	{
		return (GetSourceState() == AL_PAUSED);
	}
	
	
	//
	// Déterminer si le son est arrêté
	bool Sound::IsStopped()
	{
	
		// Déclarations
		ALenum state;
		
		// Récupération de l'état
		state = GetSourceState();
		
		// Retour
		return (state == AL_STOPPED || state == AL_INITIAL);
	
	}
	
	
	//
	// Retourner le temps de lecture en secondes
	float Sound::GetPlayTime()
	{
		
		// Déclarations
		ALfloat value;
		
		alGetSourcef(source, AL_SEC_OFFSET, &value);
		
		return value;
		
	}
	
	
	//
	// Positionner la lecture au temps spécififé en secondes
	void Sound::SetPlayTime(float value)
	{
		alSourcef(source, AL_SEC_OFFSET, max(0.0f, min(GetDuration(), value)));
	}
	
	
	//
	// Obtenir le volume
	float Sound::GetVolume()
	{
	
		// Déclarations
		ALfloat value;
		
		// Récupération
		alGetSourcef(source, AL_GAIN, &value);
		alGetError();
		
		// Retour
		return value;
		
	}
	
	
	//
	// Définir le volume
	void Sound::SetVolume(float value)
	{
		alSourcef(source, AL_GAIN, value);
		alGetError();
	}
	
	
	//
	// Obtenir la position du son dans l'espace
	void Sound::GetPosition(float position[3])
	{
		alGetSourcefv(source, AL_POSITION, position);
		alGetError();
	}
	
	
	//
	// Définir la position du son dans l'espace
	void Sound::SetPosition(float position[3])
	{
		alSourcefv(source, AL_POSITION, position);
		alGetError();
	}
	
	void Sound::SetPosition(float x, float y, float z)
	{
		alSource3f(source, AL_POSITION, x, y, z);
		alGetError();
	}


	//
	// Obtenir le pitch du son
	float Sound::GetPitch()
	{
	
		// Déclarations
		ALfloat value;
		
		// Récupération
		alGetSourcef(source, AL_PITCH, &value);
		alGetError();
		
		// Retour
		return value;
		
	}
	
	
	//
	// Définir le pitch du son
	void Sound::SetPitch(float value)
	{
		alSourcef(source, AL_PITCH, value);
		alGetError();
	}

