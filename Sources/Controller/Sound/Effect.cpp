#include "Effect.h"	

// *********************************************************************************************************************
	
	
	//
	// Constructeur protégé avec données et infos
	Effect::Effect(
		void* data,
		int samplesCount,
		int sampleRate,
		int channelsCount,
		bool lowPrecision
	) : Sound(sampleRate, channelsCount, (float)samplesCount / sampleRate)
	{
	
		// Déclarations
		int sampleSize;
		ALenum format;
	
	
		// Création du buffer
		alGenBuffers(1, &buffer);
		if (alGetError() != AL_NO_ERROR)
			throw string("Effect::ctor() Unable to create buffer");
	
	
		//Calcul de la taille des samples et du format OpenAL correspondant
		if (lowPrecision)
		{
			sampleSize = channelsCount;
			format = (channelsCount == 1 ? AL_FORMAT_MONO8 : AL_FORMAT_STEREO8);
		}
		else
		{
			sampleSize = 2 * channelsCount;
			format = (channelsCount == 1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16);
		}
		
		
		// Remplissage du buffer avec les données et association à la source
		alBufferData(buffer, format, data, (sampleSize * samplesCount), sampleRate);
		alSourcei(GetSource(), AL_BUFFER, buffer);
		alGetError();
	
	}


// *********************************************************************************************************************

	
	//
	// Destructeur
	Effect::~Effect()
	{
		
		// Arrêt de la lecture
		Stop();
		
		// Détachement du buffer de la source
		alSourcei(GetSource(), AL_BUFFER, 0);
		
		// Nettoyage
		alDeleteBuffers(1, &buffer);
		alGetError();
		
	}
	

	//
	// Créer un effet depuis un fichier vorbis
	Effect* Effect::FromVorbisFile(string filename, bool lowPrecision)
	{
		
		// Déclarations
		int sampleSize, samplesCount;
		ALbyte* data = NULL;
		Effect* effect = NULL;
		VorbisStream *stream = NULL;
		
		
		// Sécurité
		try
		{
		
			// Ouverture du flux
			stream = new VorbisStream(filename);
			
			// Calcul de la taille d'un sample
			sampleSize = stream->GetChannelsCount() * (lowPrecision ? 1 : 2);
			
			// Allocation de l'emplacement de stockage des samples
			data = new ALbyte[sampleSize * stream->GetSamplesCount()];
			
			// Lecture des samples
			samplesCount = stream->ReadSamples(data, stream->GetSamplesCount(), lowPrecision);
			
			// Création de l'effet
			effect = new Effect(data, samplesCount, stream->GetSampleRate(), stream->GetChannelsCount(), lowPrecision);
			
			// Nettoyage
			delete[] data;
			delete stream;
			data = NULL;
			stream = NULL;
		
		}
		catch (string exc)
		{
			if (data != NULL) delete[] data;
			if (stream != NULL) delete stream;
			if (effect != NULL) delete effect;
			throw;
		}
		
		// Retour de l'effet
		return effect;
		
	}
	
	