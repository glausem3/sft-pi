#include "Music.h"	

// *********************************************************************************************************************
	
	
	//
	// Constructeur protégé avec flux vorbis
	Music::Music(VorbisStream *stream) : Sound(
		stream->GetSampleRate(),
		stream->GetChannelsCount(),
		(float)stream->GetSamplesCount() / stream->GetSampleRate())
	{
	
		// Mise à jour des champs
		this->stream = stream;
		this->streamingEnable = false;
		this->looping = false;
		
		// Génération des buffers
		alGenBuffers(STREAMING_BUFFERS_COUNT, streamingBuffers);
		if (alGetError() != AL_NO_ERROR)
			throw string("Music::ctor() Unable to create buffers");
	
		// Initialisation du buffer de données pour le décodage
		dataBuffer = new ALbyte[stream->GetSampleRate() * stream->GetChannelsCount() * 2 * STREAMING_BUFFERS_DURATION / 1000];
	
	}


	//
	// Logique du thread de streaming
	void* Music::Streaming_Thread(void* arg)
	{
	
		// Déclarations
		int processed;
		ALuint buffer;
		Music *instance = (Music*)arg;
		
		
		// Boucle de travail
		while (instance->streamingEnable)
		{
		
			// Récupération du nombre de buffers traités
			alGetSourcei(instance->GetSource(), AL_BUFFERS_PROCESSED, &processed);
			
			// Remplissage des buffers vides
			for (int i=0; i<processed; i++)
			{
			
				// Récupération du buffer a remplir
				alSourceUnqueueBuffers(instance->GetSource(), 1, &buffer);
				
				// Remplisssage
				if (!instance->StreamingFillBuffer(buffer))
				{
					if (!instance->looping) return NULL;
					instance->stream->SeekTime(0.0f);
					instance->StreamingFillBuffer(buffer);
				}
				
				// Remise en file du buffer
				alSourceQueueBuffers(instance->GetSource(), 1, &buffer);
				
			}
			
			// Temps d'attente
			if (processed == 0)
				usleep(1000 * STREAMING_BUFFERS_DURATION);
			else if (processed <= 1)
				usleep(1000 * STREAMING_BUFFERS_DURATION / 2);
			else if (processed <= 2)
				usleep(1000 * STREAMING_BUFFERS_DURATION / 4);
			else
				usleep(1000);
				
		}
		
	
		// Retour OK
		return NULL;
		
	}
	

	//
	// Remplir un buffer de streaming
	bool Music::StreamingFillBuffer(ALuint buffer)
	{
		
		// Déclarations
		int result;
		
		
		// Lecture des samples
		result = stream->ReadSamples(
			dataBuffer,
			stream->GetSampleRate() * STREAMING_BUFFERS_DURATION / 1000,
			false
		);
		
		// Fin de fichier
		if (result == 0)
			return false;
	
		// Mise à jour du buffer
		alBufferData(
			buffer,
			(stream->GetChannelsCount() == 2 ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16),
			dataBuffer,
			result * stream->GetChannelsCount() * 2,
			stream->GetSampleRate()
		);
		alGetError();
		
	
		// Retour OK
		return true;
	
	}


	//
	// Démarrer le thread de streaming
	void Music::StreamingStart()
	{
		
		// Déjà démarré
		if (streamingEnable) return;
		
		// Préparation des buffers
		for (int i=0; i<STREAMING_BUFFERS_COUNT; i++)
			StreamingFillBuffer(streamingBuffers[i]);
	
		// Mise en file des buffers
		alSourceQueueBuffers(GetSource(), STREAMING_BUFFERS_COUNT, streamingBuffers);
		alGetError();
		
		// Lancement du thread de streaming
		streamingEnable = true;
		pthread_create(&streamingThread, NULL, Streaming_Thread, this);
	
	}
	
	
	//
	// Arrêter le thread de streaming
	void Music::StreamingStop()
	{
		
		// Déjà arrêté
		if (!streamingEnable) return;
		
		// Arrêt du thread de streaming
		streamingEnable = false;
		pthread_join(streamingThread, NULL);
		
		// Sortie des buffers de la file
		alSourcei(GetSource(), AL_BUFFER, 0);
		
	}
	


// *********************************************************************************************************************

	
	//
	// Destructeur
	Music::~Music()
	{
	
		// Arrêt de la lecture
		Stop();
		
		// Nettoyage des buffers
		alDeleteBuffers(STREAMING_BUFFERS_COUNT, streamingBuffers);
		
		// Nettoyage
		delete[] dataBuffer;
		delete stream;
		
	}
	
	
	//
	// Lancer la lecture
	void Music::Play(bool looping)
	{
		
		// Arrêt de la lecture
		Stop();
		
		// Lancement du streaming
		stream->SeekTime(0.0f);
		StreamingStart();
	
		// Lancement de la lecture
		this->looping = looping;
		alSourcePlay(GetSource());
		alGetError();
	
	}
	
	
	//
	// Arrêter la lecture
	void Music::Stop()
	{
	
		// Arrêt de la lecture
		alSourceStop(GetSource());
		alGetError();
	
		// Arrêt du streaming
		StreamingStop();
		
	}
	
	
	//
	// Retourner le temps de lecture en secondes
	//
	float Music::GetPlayTime()
	{
		throw string("Music::GetPlayTime() unsupported operation");
	}
	
	
	//
	// Positionner la lecture au temps spécififé en secondes
	//
	void Music::SetPlayTime(float value)
	{
		throw string("Music::SetPlayTime() unsupported operation");
	}
	

	//
	// Créer une musique depuis un fichier vorbis
	Music* Music::FromVorbisFile(string filename)
	{
		return new Music(new VorbisStream(filename));
	}

	