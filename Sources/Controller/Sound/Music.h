#pragma once

// Includes standards
#include <iostream>
#include <algorithm>
#include <string>
#include <unistd.h>
#include <pthread.h>

// Includes pour OpenAL
#include <AL/al.h>
#include <AL/alc.h>

// Includes locaux
#include "Sound.h"
#include "VorbisStream.h"

using namespace std;

// *********************************************************************************************************************



//
// Classe représentant une musique (son lu en streaming)
//
class Music : public Sound
{
	
	// Constantes de paramétrage
	static const int STREAMING_BUFFERS_COUNT = 4;
	static const int STREAMING_BUFFERS_DURATION = 250;
	
	
	// Buffers de lecture
	ALuint streamingBuffers[STREAMING_BUFFERS_COUNT];
	
	// Buffer de données pour le décodage des samples
	ALbyte* dataBuffer;
		
	// Flux vorbis contenant les données
	VorbisStream* stream;
	
	// Thread de streaming
	pthread_t streamingThread;

	// Flag d'arrêt du thread de streaming
	volatile bool streamingEnable;
	
	// Flag de bouclage
	volatile bool looping;


	//
	// Logique du thread de streaming
	static void* Streaming_Thread(void* instance);

	
	//
	// Remplir un buffer de streaming
	//
	//   + Retour
	//     vrai si le buffer a pu être rempli (partiellement ou complètement)
	//
	bool StreamingFillBuffer(ALuint buffer);
	
	
	//
	// Lancer le thread de streaming
	void StreamingStart();
	
	
	//
	// Arrêter le thread de streaming
	void StreamingStop();


// *********************************************************************************************************************
protected:
	
	
	//
	// Constructeur protégé avec flux vorbis
	Music(VorbisStream *stream);


// *********************************************************************************************************************
public:


	//
	// Destructeur
	virtual ~Music();


	//
	// Lancer la lecture
	virtual void Play(bool looping = false);
	
	
	//
	// Arrêter la lecture
	virtual void Stop();


	//
	// Retourner le temps de lecture en secondes
	//
	virtual float GetPlayTime();
	
	
	//
	// Positionner la lecture au temps spécififé en secondes
	//
	virtual void SetPlayTime(float value);


	//
	// Créer une musique depuis un fichier vorbis
	//
	// + Paramètres
	//     <filename>       nom du fichier
	//
	static Music* FromVorbisFile(string filename);


};
