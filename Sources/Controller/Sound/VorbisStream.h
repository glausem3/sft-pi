#pragma once


// Includes standards
#include <iostream>
#include <string>
#include <cstring>

// Includes pour la lib vorbis
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisenc.h>
#include <vorbis/vorbisfile.h>

// Includes pour OpenAL
#include <AL/al.h>
#include <AL/alc.h>

using namespace std;


// *********************************************************************************************************************


//
// Classe représentant un flux sur un fichier vorbis
//
class VorbisStream
{

	// Handle vers le fichier source
	FILE* fileHandle;
	
	// Handle vers le flux décodé
	OggVorbis_File oggHandle;
	
	// Nombre de samples contenus dans le flux
	int samplesCount;
	
	// Nombre de canaux
	int channelsCount;
	
	// Taux d'échantillonage
	int sampleRate;
	

// *********************************************************************************************************************
public:

	//
	// Constructeur avec nom de fichier source
	VorbisStream(string filename);
	
	
	//
	// Destructeur
	~VorbisStream();
	
	
	//
	// Lire des samples depuis le fichier et les stocker en format PCM
	//
	//   + Paramètres
	//     <target>         pointeur sur l'emplacement cible des samples lus
	//     <count>          nombre de samples à lire
	//     <lowPrecision>   si vrai les samples retournés sont en PCM 8 bits, sinon en PCM 16 bits
	//
	//   + Retour
	//     Nombre de samples effectivement lus ou zéro si le fichier est terminé. A noter que le nombre de de samples
	//     lus peut être inférieur à <count> si le fichier n'en contient pas suffisement.
	//
	//   + Remarques
	//     L'emplacement cible doit être de taille <count>*GetChannels() octets minimum si le paramètre <lowPrecision>
	//     est vrai et 2*<count>*GetChannels() si le paramètre est faux (par défaut)
	//
	int ReadSamples(void* target, int count, bool lowPrecision=false);
	
	
	//
	// Déplacer le pointeur de flux aux temps spécifié (en secondes)
	void SeekTime(float time)
	{
		ov_time_seek(&oggHandle, time);
	}
	
	
	//
	// Déplacer le pointeur de flux au sample spécifié
	void SeekSamples(int sample)
	{
		ov_pcm_seek(&oggHandle, sample * channelsCount);
	}
	

	//
	// Obtenir une chaîne descriptive pour un code d'erreur vorbis
	string GetErrorString(int code);
	
	
	//
	// Obtenir le nombre de samples
	int GetSamplesCount() { return samplesCount; }
	
	
	//
	// Obtenir le nombre de canaux
	int GetChannelsCount() { return channelsCount; }
	
	
	//
	// Obtenir le taux d'échantillonnage
	int GetSampleRate() { return sampleRate; }
	
	
	
	float GetCurrentTime()
	{
		return (float)ov_time_tell(&oggHandle);
	}
	
	int GetCurrentSample()
	{
		return ov_pcm_tell(&oggHandle);
	}
	
	
};
