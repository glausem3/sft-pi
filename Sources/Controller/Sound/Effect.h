#pragma once

// Includes standards
#include <iostream>
#include <algorithm>
#include <string>
#include <unistd.h>

// Includes pour OpenAL
#include <AL/al.h>
#include <AL/alc.h>

// Includes locaux
#include "Sound.h"
#include "VorbisStream.h"

using namespace std;

// *********************************************************************************************************************



//
// Classe représentant un effet (son entièrement contenu dans la mémoire)
//
class Effect : public Sound
{
	
	// Buffer contenant les samples
	ALuint buffer;


// *********************************************************************************************************************
protected:
	
	
	//
	// Constructeur protégé avec données et infos
	Effect(
		void* data,
		int samplesCount,
		int sampleRate,
		int channelsCount,
		bool lowPrecision=false
	);


// *********************************************************************************************************************
public:


	//
	// Destructeur
	virtual ~Effect();


	//
	// Créer un effet depuis un fichier vorbis
	//
	// + Paramètres
	//     <filename>       nom du fichier
	//     <lowPrecision>   si vrai l'effet est créé en PCM 8 bits, sinon en PCM 16 bits
	//
	static Effect* FromVorbisFile(string filename, bool lowPrecision=false);


};
