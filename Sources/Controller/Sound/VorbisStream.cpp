#include "VorbisStream.h"

// *********************************************************************************************************************


	//
	// Constructeur avec nom de fichier source
	VorbisStream::VorbisStream(string filename)
	{
	
		// Déclarations
		int result;
		vorbis_info *infos;
		
	
		// Ouverture du fichier
		fileHandle = fopen(filename.c_str(), "rb");
		if (!fileHandle)
			throw string("Impossible d'ouvrir le fichier source: ") + filename;
	
		// Création du flux de décodage
		result = ov_open(fileHandle, &oggHandle, NULL, 0);
		if (result < 0)
		{
			fclose(fileHandle);
			throw string("Erreur lors de l'ouverture du flux de décodage: ") + GetErrorString(result);
		}
	
		// Récupération des infos sur le flux
		infos = ov_info(&oggHandle, -1);
		channelsCount = infos->channels;
		sampleRate = infos->rate;
		samplesCount = ov_pcm_total(&oggHandle, -1);
	
	}
	
	
	//
	// Destructeur
	VorbisStream::~VorbisStream()
	{
		fileHandle = NULL;
		ov_clear(&oggHandle); // note: ferme le flux standard associé..
	}


	//
	// Lire des samples depuis le fichier et les stocker en format PCM
	int VorbisStream::ReadSamples(void* target, int count, bool lowPrecision)
	{
	
		// Déclarations
		int section;
		int wordSize, sampleSize;
		int offset, result;
		char* dataPtr;
	
	
		// Initialisation
		offset = 0;
		dataPtr = (char*)target;
		wordSize = (lowPrecision ? 1 : 2);
		sampleSize = wordSize * channelsCount;
		
		// Boucle de travail
		while (offset < count)
		{
		
			// Lecture des samples
			result = ov_read(
				&oggHandle,
				dataPtr,
				(count - offset) * sampleSize,
				0,
				wordSize,
				1,
				&section
			);
			
			
			// Erreur de lecture
			if (result < 0)
				throw string("VorbisStream::ReadSamples() Error while reading from stream: " + result);
		
			// Fin du fichier
			if (result == 0)
				break;
		
			// Mise à jour
			offset += (result / sampleSize);
			dataPtr += result;
		
		}
		
		// Retour du nombre de samples lus
		return offset;
	
	}
	

	//
	// Obtenir une chaîne descriptive d'une erreur vorbis
	string VorbisStream::GetErrorString(int code)
	{
	    switch(code)
	    {
	        case OV_EREAD:
	            return string("Read from media.");
	        case OV_ENOTVORBIS:
	            return string("Not Vorbis data.");
	        case OV_EVERSION:
	            return string("Vorbis version mismatch.");
	        case OV_EBADHEADER:
	            return string("Invalid Vorbis header.");
	        case OV_EFAULT:
	            return string("Internal logic fault (bug or heap/stack corruption.");
	        default:
	            return string("Unknown Ogg error.");
	    }
	}
	
