#pragma once

// Includes standards
#include <iostream>
#include <string>
#include <unistd.h>

// Includes pour OpenAL
#include <AL/al.h>
#include <AL/alc.h>

using namespace std;

// *********************************************************************************************************************


//
// Classe d'accès à OpenAL
//
class OpenAL
{
	
	// Device système
	static ALCdevice *device;
	
	// Contexte global
	static ALCcontext *context;

	// Flags d'état
	static bool initialized;


// *********************************************************************************************************************
public:


	//
	// Initialiser OpenAL
	static void Initialize();
	
	
	//
	// Terminer et nettoyer les ressources utilisées par OpenAL
	static void Terminate();
	
	
	
	//
	// Obtenir le volume global
	static float GetGlobalVolume();
	
	
	//
	// Définir le volume global
	static void SetGlobalVolume(float value);
	
	
	//
	// Obtenir la position de l'écouteur dans l'espace
	static void GetListenerPosition(float position[3]);
	
	
	//
	// Définir la position de l'écouteur dans l'espace
	static void SetListenerPosition(float position[3]);
	static void SetListenerPosition(float x, float y = 0.0f, float z = 0.0f);
	

	//
	// Obtenir la vélocité de l'écouteur dans l'espace
	static void GetListenerVelocity(float velocity[3]);
	
	
	//
	// Définir la vélocité de l'écouteur dans l'espace
	static void SetListenerVelocity(float velocity[3]);
	static void SetListenerVelocity(float x, float y = 0.0f, float z = 0.0f);
	
};
