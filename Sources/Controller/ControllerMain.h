#pragma once

// Includes standards
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <signal.h>

// Includes locaux
#include "Controller.h"

using namespace std;


// *********************************************************************************************************************


	//
	// Programme principal
	int main(int argc, const char** argv);
	

	//
	// Handler pour les signaux de terminaison
	void SignalHandler(int signal);
	
	