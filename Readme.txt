	

Mise en place de l'environnement logiciel
------------------------------------------------------------------------------------------------------------------------

# Installation du Raspberry PI

	1. Télécharger l'image "Raspbian Wheezy" depuis raspberrypi.org et la copier sur la carte SD
	
	2. Procéder à l'installation initiale, comme décrit dans les informations d'installation sur le site du
	   raspberry. Si la commande de configuration raspi-config ne s'est pas lancée d'elle même, le faire
	   manuellement.
	   
	3. Modifier le mode de démarrage afin de désactiver l'interface graphique (optionnel, mais préférable..)
	
	4. Activer le serveur SSH et configurer l'interface réseau selon l'environnemnent de travail. A noter qu'un
	   accès internet est nécessaire au niveau du raspberry pi pour l'installation des différentes librairies


# Installation de phidgets

	Le capteur de lumière est accédé par l'intermnédiaire de la librairie phidgets, il faut donc la copier et la
	compiler

	1. Télécharger et dézipper la distribution depuis le site
	
	2. Aller dans le répertoire et exécuter
		./configure
		make
		sudo make install
		(Note: le make prend un temps de dingue sur le raspberry)
		
	3. Si un IDE est utilisé il faudra ajouter la libraire phidget21 au linker
	
	
# Installation d'OpenAL

	La sortie son de l'application est faite grâce à OpenAL. La librairie de développement n'est pas installée par
	défaut sur le raspberry, il faut le faire manuellement

	1. Cloner le repository d'OpenAL
		git clone git://repo.or.cz/openal-soft.git openal-soft
		
	2. Installer les paquets de développement Pulse et ASound2
		sudo apt-get install libpulse-dev
		sudo apt-get install libasound2-dev
		
	3. Compiler OpenAL en allant de le répertoire build du repository
		cmake .. -DLIBTYPE=STATIC
		make
		sudo make install
		
	4. Copier la librairie statique dans /usr/lib
		sudo cp libopenal.a /usr/lib
		
	5. Si un IDE est utilisé il faudra ajouter la libraire openal au linker
  
  
# Liens utiles

	+ Documentation de la librairie phidgets
	  http://www.phidgets.com/documentation/web/cdoc/index.html
	
	+ Repository officiel d'OpenAL
	  git://repo.or.cz/openal-soft.git openal-soft
	
	+ Explications pour la compilatio d'OpenAL avec le support d'ALSA et PulseAudio
	  https://github.com/blackberry/GamePlay/issues/642
	
	+ Explications sur la façon de configurer l'interface réseau du raspberry avec les fichiers de config
	  http://www.mathworks.ch/ch/help/simulink/ug/getting-the-raspberry_pi-ip-address.html
	
	
# Commandes utiles

	+ Réglage du volume global de la carte son
      amixer cset numid=1 -- 20%



Utilisation du code
------------------------------------------------------------------------------------------------------------------------

	L'application est entièrement réalisée en C++, afin de pouvoir la compiler il faut disposer d'un' compilateur
	compatible (par exemple g++). Pour utiliser g++, il faut l'installer manuellement car il n'est pas compris dans
	la distribution raspbian de base.
	
	A noter qu'il est important de recompiler l'application directement sur le raspberry et non sur une machine de
	bureau, pour éviter les problèmes d'architecture. (ARM vs x86)
	
	Les tests peuvent être faits dans un environnement de développement (testé avec monodevelop) ou alors directement
	avec le makefile fourni.

