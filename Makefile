##
## Paramètres globaux
##

GLOBAL_FLAGS=-Wall
#GLOBAL_FLAGS=-Wall -O3


##
## Paramètres du contrôleur
##

CONTROLLER_PATH=./Sources/Controller/

CONTROLLER_OUTPUT=Controller.out

CONTROLLER_LIBS=-lphidget21 -lm -lopenal -lvorbis -lvorbisfile -lpthread

CONTROLLER_SRC=$(wildcard $(CONTROLLER_PATH)*.cpp) $(wildcard $(CONTROLLER_PATH)Phidget/*.cpp) $(wildcard $(CONTROLLER_PATH)Sound/*.cpp)
CONTROLLER_OBJ=${CONTROLLER_SRC:.cpp=.o}



##
## Cible par défaut
##
#all: launcher controller
all: controller


##
## Cible du contrôleur
##
controller: ${CONTROLLER_OBJ}
	g++ ${CONTROLLER_OBJ} -o ${CONTROLLER_OUTPUT} ${CONTROLLER_LIBS}

	
##
## Cible de génération des fichiers objets
##
%.o: %.cpp
	g++ -c -o $@ $< $(GLOBAL_FLAGS)
%.o: %.c
	g++ -c -o $@ $< $(GLOBAL_FLAGS)


##
## Cibles d'installation
##
#install:
#	echo "#!/bin/bash" > .sh
#	echo "pushd \"$(PWD)\"" >> bootstrap.sh
#	echo "./Launcher.out" >> bootstrap.sh
#	echo "popd" >> bootstrap.sh
#	chmod +x bootstrap.sh
#	echo "@reboot $(PWD)/bootstrap.sh" | crontab

#uninstall:
#	echo "@reboot $(PWD)/bootstrap.sh" | crontab -r


##
## Cible de nettoyage
##
clean:
	rm -f ${CONTROLLER_OBJ}
	rm -f ${CONTROLLER_OUTPUT}
	rm -f *.o
	rm -f *.d
